// after the text semicolon is not that mandatory but for better practise we use semicolon at the end.
console.log("hello world") 
// you can use single or double or  backtick but mostly use double quotes we use backtick in the case of string templates


// variables in javascript (other way:we have to use let here instead of var)

 var Firstname="Dhileep"
// var is variable declaration and Firstname is variable name which is case-sensitive
console.log(Firstname);
// variable name is case-sensitive if you not given same variable name as given above then it shows error
Firstname="Kumar"
console.log(Firstname);
// it is not required variable declaration can be done at all time.

// let keyword:it is similar to the var keyword but for future purpose we mostly use let.it can be only used at once in initilization if we use again it goes error
Firstname="Sathelli";
console.log(Firstname);

// constants:constant value cannot be changed but can be manipulated

const x=3.14;
console.log(x);
console.log(x*2)

// strings

// Indexing(last index=length-1)
Firstname="   Dhileep   ";
console.log(Firstname[4]);
console.log(Firstname.length);
console.log(Firstname[Firstname.length-5]);
// space also considered in length of string.inorder to remove it we use trim.
console.log(Firstname.length);
newString=Firstname.trim();
console.log(newString);
console.log(newString.length);
newString=newString.toUpperCase();
console.log(newString);
newString=newString.toLowerCase();
console.log(newString);

// slicing (start index )(end index)
newString=newString.slice(1,8); //hile
console.log(newString); 
newString="kumar";
newString=newString.slice(1); //
console.log(newString); 
newString="sathelli";
// Datatypes
let age=22;
let newname="dhileep"
console.log(typeof age);
console.log(typeof Firstname);
console.log(typeof (age + "")); //converting number to string
let myStr =+"22";
console.log(typeof myStr); //converting string to number

age=String(age);
console.log(typeof age);
// concatnation
let string1="Dhileep";
let string2="kumar";
let fullname=string1 + " " +string2;
console.log(fullname);
let num1="10";
let num2="20";
let num3= +num1 + +num2;
console.log(num3);
let num4="10";
let num5="20";
let num6= num4 + num5;
console.log(num6);
// template string
let myage=22;
let Name="Dhileep";
let aboutme="my name is " + Name + " and my age is " + myage ;
console.log(aboutme);

let num7=17;
let num8=19;
console.log(num7>num8);  //false
console.log(num7==num8); //false
//(== it only checks the value not the datatype)
console.log(num7!=num8); //true
console.log(num7 !==num8); //true

// conditional statements
if(num7<num8){
    console.log("Not elegible to vote")
}else {
    console.log("elegible to vote")
}

//ternary operator
let value=15;
let drink;
if(value>=10){
    drink="Milk"
}else {
    drink="Tea"
}
console.log(drink);

//and or operator

let first_name="Dhileep";
let my_age=22;
if(first_name[0] ==="D" && my_age>19){
    console.log("name starts with D and above 19");
}else {
    console.log("inside else");
}
if(first_name[0] ==="d" || my_age<19){
    console.log("name starts with D and above 19");
}else {
    console.log("inside else");
}

//nested if-else
let winningnumber =20;
let guessnumber=+prompt("guess a number");
if(guessnumber ===winningnumber){
    console.log("correct");
}else{
    if(guessnumber < winningnumber){
        console.log("too low");
    }else{
        console.log("too high");
    }
}

let vote_age=18;
if(vote_age<16){
    console.log("not eligible");
}else if(vote_age<15){
    console.log("you are not eligible");
}else if(vote_age<20){
    console.log("yes,you are eligile to vote");
}else{
    console.log("eligible")
}
console.log("Hello");

//switch statement

let day=4;
switch(day){
    case 0:
        console.log("sunday");
        break;
    case 1:
        console.log("monday");
        break;
    case 2:
        console.log("tuesday");
        break;
    case 3:
        console.log("wednesday");
        break;
    case 4:
        console.log("thursday");
        break;
    case 5:
        console.log("friday");
        break;
    case 6:
        console.log("saturday");
        break;
    default:
        console.log("not counted");
}

//while loop
let num=0;
while(num<=5){
    console.log(num);
    num++;
}

//for loop

for(i=0;i<9;i++){
    console.log(i);
}
//sum of 100 natural numbers using for loop
let value_1=0;
for(i=1;i<=100;i++){
    value_1=value_1+i;
}
console.log(value_1);

//break keyword:it checks until the condition in the loop gets true ,if its true then it exits from the loop
for(i=0;i<=10;i++){
    if(i===5){
        break;
    }
    console.log(i);
}
//continue keyword: it is used when we want skip the particular condition and continue the rest execution
for(i=0;i<=10;i++){
    if(i>5){
        continue;
    }
    console.log(i);
}
//do while loop:here first do statements executes and then it checks the while statement
let j=0;
do{
    console.log(j);
    i++;
}while(j<=9);








