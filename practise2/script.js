//arrays:collection of ordered items
let fruits=["pineapple","orange","papaya"];
console.log(fruits);
let string=["Dhileep","Bunny","Akhila"];
let mixed=[1,4,25,5,"string",null,undefined];
console.log(mixed);
console.log(fruits[2]);
let fruit=["apple","banana","mango"];
console.log(fruit);
fruit[1]="orange";
console.log(fruit);
console.log(typeof fruit); //object
let obj={}//object literal
console.log(Array.isArray(fruit));
console.log(Array.isArray(obj));
//array push pop
console.log(fruit);
fruit.push("cherry");
console.log(fruit);
//array pop
console.log(fruit.pop());
console.log(fruit);
fruit.unshift("cherry"); //unshift it will add it at 1st element
console.log(fruit);
//shift:it will remove the 1st element in the array
let fruit_s=fruit.shift();
console.log(fruit);
console.log("removed fruit is ",fruit_s);
 //primitive vs reference datatypes:
//object is non-primitive datatype and arrays and functions are belongs to object datatype
let num1 = 6;
let num2 = num1;
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);
num1++;
console.log("after incrementing num1")
console.log("value is num1 is", num1);
console.log("value is num2 is", num2);
// reference types :Datatype object holds key-value pairs with an unordered collection of data.
let array1 = ["item1", "item2"];
let array2 = array1;
console.log("array1", array1);
console.log("array2", array2);
array1.push("item3");
console.log("after pushing element to array 1");
console.log("array1", array1);
console.log("array2", array2);
//cloning array:we can use slice or concat for cloning the array
let array3 = ["item1", "item2"];
let array = array1.slice(0).concat(["item3", "item4"]);
let array4 = [].concat(array1,["item3", "item4"]);
console.log(array);
console.log(array4);
//spread operator
let array5=[...array1,"item3","item4"];
array1.push("item3");
console.log(array1===array5);
console.log(array1);
console.log(array2);
//for loop in array
let Fruits=["apple","banana","mango","grapes"];
for(let i=0;i<=9;i++){
    console.log(i);

}
console.log(Fruits.length);
console.log(Fruits[Fruits.length-1]);
let Fruit=[];
for(let i=0;i<Fruits.length;i++){
    Fruit.push(Fruits[i].toUpperCase());

}
console.log(Fruit);


const Fruit_s = ["apple", "mango"]; 
fruits.push("banana");
console.log(Fruit_s)

//while loop in array
const fruit$=["apple","banana","mango"];
let i=0;
while(i<fruit$.length){
    console.log(fruit$[i]);
    i++;
}
const fruitss=["apple","banana","mango"];
const Fruitss=[];
let j=0;
while(j<Fruitss.length){
    Fruitss.push(fruitss[j].toUpperCase());
    j++;
}
console.log(Fruitss);
//for of loop in array
const value = ["apple", "mango", "grapes", "fruit4", "fruit5"];
const value1= [];

for(let value_s of value1){
    value1.push(value_s.toUpperCase());
}
console.log(value1);

for(let i = 0; i<value.length; i++){
    console.log(value[i]);
}
//array destructuring
const myArray=["value1","value2","value3","value4"];
let myvar1=myArray[0];
let myvar2=myArray[1];
console.log("value of myvar1",myvar1);
console.log("value of myvar2",myvar1);
let [myvar3,myvar4, ...myNewArray]=myArray;
console.log("valueof myvar3",myvar3);
console.log("valueof myvar4",myvar4);
console.log(myNewArray);






 





