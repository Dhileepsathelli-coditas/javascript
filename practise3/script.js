//objects are real world entities and it store key-value pairs
const person={name:"Dhileep",age:22};
console.log(person);
console.log(person.name);
console.log(person.age);
person.gender="male";
console.log(person);
// difference between dot and bracket notaion
const key = "email";
const mine = {
    name: "Dhileep",
    age: 22,
    "mine hobbies": ["photography", "Basketball", "Volley ball"]

}

console.log(mine["mine hobbies"]);
mine[key] = "sathelli777@gmail.com";
console.log(mine);
// how to iterate object 
const Person = {
    name: "Dhileep",
    age: 22,
    "Person hobbies": ["photography", "Basketball", "Volley ball"]

}

// for in loop 
// Object.keys 

for(let key in Person){
    // console.log(`${key} : ${person[key]}`);
    console.log(key," : " ,Person[key]);
}

console.log(typeof (Object.keys(Person)));
const val = Array.isArray((Object.keys(Person)));
console.log(val);

for(let key of Object.keys(Person)){
    console.log(Person[key]);
}
