// computed properties

const key1 = "objkey1";
const key2 = "objkey2";

const value1 = "myvalue1";
const value2 = "myvalue2";

// const obj = {
//     objkey1 : "myvalue1",
//     objkey2 : "myvalue2",
// }

const obj = {
    [key1] : value1,
    [key2] : value2

}

// const obj = {};

// obj[key1] = value1;
// obj[key2] = value2;
// console.log(obj);

// spread operator
const array1 = [1, 2, 3];
const array2 = [5, 6, 7];

// const newArray = [...array1, ...array2, 89, 69];
const newArray = [..."123456789"];
console.log(newArray);

// spread operator in objects
const obj1 = {
    key1: "value1",
    key2: "value2",
  };
  const obj2 = {
    key1: "valueUnique",
    key3: "value3",
    key4: "value4",
  };
  
  const newObject = { ...obj2, ...obj1, key69: "value69" };
//   const newObject = { ...["item1", "item2"] };
//   const newObject = { ..."abcdefghijklmnopqrstuvwxyz" };
  console.log(newObject);

  //object destructiring
  const band={
    bandname:"DSP",famousSong:"Jalsa"
  };
  const { bandname, famousSong }=band;
  console.log(bandname); 

//objects inside array
const users =[
    {userId:1,firstName:"Dhileep",gender:'male'},
    {userId:2,firstName:"Lasya",gender:'female'},
    {userId:3,firstName:"Abhishek",gender:'male'},

]
console.log(users);
for(let user of users){
    console.log(user.firstName);
}

//nested destructuring

const user =[
    {userId:1,firstName:"Dhileep",gender:'male'},
    {userId:2,firstName:"Lasya",gender:'female'},
    {userId:3,firstName:"Abhishek",gender:'male'},

]
const [{firstName}, ,{gender}]=user;
console.log(firstName);
console.log(gender);
