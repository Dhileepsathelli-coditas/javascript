//functions:it is a block of code simply it runs when it is called
function sumThreeNumbers(number1, number2, number3){
    return number1 + number2 + number3;
}
console.log(sumThreeNumbers(1,5,3));

function isEven(number){
    return number % 2 === 0;
}
console.log(isEven(4));
function firstChar(anyString){
    return anyString[0];
}

console.log(firstChar("zbc"));



function findTarget(array, target){
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}
const myArray = [1,3,8,90]
const ans = findTarget(myArray, 4);
console.log(ans);

const sum = function(number1, number2, number3){
    return number1 + number2 + number3;
}
const ans1 = sum(5,3,6);
console.log(ans1);


// function using const
const isEvenNum = function(number){
    return number % 2 === 0;
}
console.log(isEvenNum(2));

const FirstChar = function(anyString){
    return anyString[0];
}
console.log(FirstChar("dhileep"));

const find_Target = function(array, target){
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}




// arrow functions


const helloWorld = () => {
    console.log("helloWorld ......");
}

helloWorld();

const sum_ThreeNumbers = (number1, number2, number3) => {
    return number1 + number2 + number3;
}

const ans2 = sum_ThreeNumbers(8,5,3);
console.log(ans2);



const is_Even = number => number % 2 === 0;


console.log(is_Even(3));

const First_Char = anyString => anyString[0];

console.log(First_Char("Dhileep"));


const Find_Target = (array, target) => {
    for(let i = 0; i<array.length; i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;
}

// // functions inside function 
function app(){
    const myFunc = () =>{
        console.log("go inside function")
    }
    
    const addTwo = (num1, num2) =>{
        return num1 + num2;
    }

    const mul = (num1, num2) => num1* num2;

    console.log("inside app");
    myFunc();
    console.log(addTwo(4,5));
    console.log(mul(4,5));
}
app();


// // lexical scope 
const myVar = "value1";

function myApp(){
    function myFunc(){
        const myVar = "value59";
        const myFunc2 = () =>{
            console.log("inside myFunc", myVar);
        }
        myFunc2();
    }
    console.log(myVar);
    myFunc();
}

myApp();




// // block scope vs function scope :here let and const are block scope and var is a function scope

if(true){
    var firstName = "Dhileep";
    console.log(firstName);
}

console.log(firstName);

function myApp(){
    if(true){
        var firstName = "Dhileep";
        console.log(firstName);
    }

    if(true){
        console.log(firstName);
    }
    console.log(firstName);
}

myApp();

//defualt parametres 

function addTwo(a,b){
    if(typeof b ==="undefined"){
        b = 0;
    }
    return a+b;
}

function addTwo(a,b=0){
    return a+b;
}

const ans3 = addTwo(6, 4);
console.log(ans3);

//rest parametres
function myFunc(a,b,...c){
    console.log(`a is ${a}`);
    console.log(`b is ${b}`);
    console.log(`c is`, c);
}
myFunc(3,4,5,6,7,8,9);
function addAll(...numbers){
    let total = 0;
    for(let number of numbers){
        total = total + number;
    }
    return total;
}
const ans4 = addAll(4,5,4,2,10);
console.log(ans4);

// param destructuring 
const person = {
    firstName: "Dhileep",
    gender: "male",
    age: 22
}
function printDetails(obj){
    console.log(obj.firstName);
    console.log(obj.gender);
}
function printDetails({firstName, gender, age}){
    console.log(firstName);
    console.log(gender);
    console.log(age);
}

printDetails(person);

// callback functions 

function myFunc2(name){
    console.log("inside my func 2")
    console.log(`your name is ${name}`);
}
function myFunc(callback){
    console.log("hello ...How are you?..")
    callback("Dhileep");
}
myFunc(myFunc2);

// function returning function 

function myFunc(){
    function hello(){
        return "hello world"
    }
    return hello;
}

const ans5 = myFunc();
console.log(ans5());









