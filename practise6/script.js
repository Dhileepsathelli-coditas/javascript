// array methods 
//1st for each method
const numbers = [5,3,6,2];

function myFunc(number, index){
    console.log(`index is ${index} number is ${number}`);
}

numbers.forEach(function(number,index){
    console.log(`index is ${index} number is ${number}`);
});

numbers.forEach(function(number, index){
    console.log(number*4, index);
})

const users = [
    {firstName: "Dileep", age: 22},
    {firstName: "Akhila", age: 19},
    {firstName: "lasya", age: 22},
    {firstName: "pavan", age: 24},
]

users.forEach(function(user){
    console.log(user.firstName);
});

users.forEach((user, index)=>{
    console.log(user.firstName, index);
})

for(let user of users){
    console.log(user.firstName);
}

//map method

const number = [4,63,25,25];

const square = function(number){
    return number*number;
}

const squareNumber = numbers.map((number, index)=>{
    return index;
});
console.log(squareNumber);

const user = [
    {firstName: "Dileep", age: 22},
    {firstName: "Akhila", age: 19},
    {firstName: "lasya", age: 22},
    {firstName: "pavan", age: 24},
]

const userNames = user.map((user)=>{
    return user.firstName;
});

console.log(userNames);

//filter method

const num=[1,4,25,2,5];
const isEven=function(Number){
    return Number%2===0;

}
const evenNumbers=num.filter(isEven);
console.log(evenNumbers);


// reduce :it reduce array to the single value
const num_values= [1,2,3,4,5,10];
const sum = numbers.reduce((accumulator, currentValue)=>{
    return accumulator + currentValue;
}, 100);
console.log(sum);

const acess = [
    {productId: 1, productName: "mobile", price: 50000},
    {productId: 2, productName: "laptop", price: 33000},
    {productId: 3, productName: "tv", price: 55000},
]

const totalAmount = acess.reduce((totalPrice, currentProduct)=>{
    return totalPrice + currentProduct.price;
}, 0)

console.log(totalAmount);


const products = [
    {productId: 1, produceName: "i1",price: 500 },
    {productId: 2, produceName: "i2",price: 60060 },
    {productId: 3, produceName: "i3",price: 4200 },
    {productId: 4, produceName: "i4",price: 72000 },
    {productId: 5, produceName: "i5",price: 34300 },
]
const lowToHigh = products.slice(0).sort((a,b)=>{
    return a.price-b.price
});
const highToLow = products.slice(0).sort((a,b)=>{
    return b.price-a.price;
});
users.sort((a,b)=>{
    if(a.firstName > b.firstName){
        return 1;
    }else{
        return -1;
    }
});
console.log(users);


// find method :The find () method returns the value of the first element that passes a test.
const myArray = ["Hello", "catt", "dog", "lion"];
 function isLength3(string){
     return string.length === 3;
 }
 const a = myArray.find((string)=>string.length===3);
 console.log(ans);
const users = [
    {userId : 1, userName: "harshit"},
    {userId : 2, userName: "isjh"},
    {userId : 3, userName: "sdssish"},
    {userId : 4, userName: "dsdt"},
    {userId : 5, userName: "asaajsdk"},
];
const myUser = users.find((user)=>user.userId===3);


// every method:The every() method returns true if the function returns true for all elements.
 const id = [4,5,3,6,3,6];
 const ab = id.every((number)=>number%2===0);
 console.log(ab);
const userCart = [
    {productId: 1, productName: "mobile", price: 25051373440},
    {productId: 2, productName: "laptop", price: 28754343020},
    {productId: 3, productName: "tv", price: 23513450300},
]
const ans= userCart.every((cartItem)=>cartItem.price < 30000);
const nums = [8,9,3,5];
const ans = numbers.some((number)=>number%2===0);
 console.log(ans);
const userCarts= [
    {productId: 1, productName: "mobile", price: 22010340},
    {productId: 2, productName: "laptop", price:52034020},
    {productId: 3, productName: "tv", price:1345000},
    {productId: 3, productName: "macbook", price:12325000},
]
const answers = userCart.some((cartItem)=>cartItem.price > 100000);
console.log(ans);
const myArrayss = new Array(10).fill(0);
console.log(myArrayss);
const myArrayy = [1,2,3,4,5,6,7,8];
myArray.fill(0,2,5);
console.log(myArrayy);
// splice method
// start , delete , insert
const myAarray = ['item1', 'item2', 'item3'];
const deletedItem = myArray.splice(1, 2, "inserted item1", "inserted item2")
console.log("delted item", deletedItem);
console.log(myArray);


// some method :method perform testing and checks if atleast a single array element passes the test, implemented by the provided function

const numeric = [3,5,11,9];
const usercart = [
    {productId: 1, productName: "mobile", price: 55000},
    {productId: 2, productName: "laptop", price: 72000},
    {productId: 3, productName: "tv", price: 85000},
    {productId: 3, productName: "macbook", price: 255000},
]
const numbs = userCart.some((cartItem)=>cartItem.price > 900000);
console.log(numbs);


// fill method :by using this method we can assign the particular value to all the array elelments of a specific range.
 const My_Array = new Array(15).fill(0);
console.log(My_Array);
const MyArray = [1,2,3,4,5,6,7,8];
myArray.fill(0,4,7);
console.log(myArray);

const numerical = new array(15).fill(0);


// splice method =start , delete , insert 
const myArray = ['item1', 'item2', 'item3'];
const deletedItem = myArray.splice(1, 2);
console.log("delted item", deletedItem);
myArray.splice(1, 0,'inserted item');
const deletedItem = myArray.splice(1, 2, "inserted item1", "inserted item2")
console.log("delted item", deletedItem);
console.log(myArray);








