// new keyword
// constructor function 
function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}
const user1 = CreateUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user2 = CreateUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user3 = CreateUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
console.log(user1);
// console.log(user2.is18());




function CreateUser(firstName, lastName, email, age, address){
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.address = address;
}
CreateUser.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
CreateUser.prototype.is18 = function (){
    return this.age >= 18; 
}
CreateUser.prototype.sing = function (){
    return "la la la la ";
}
const user4 = CreateUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user5 = CreateUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user6 = CreateUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
for(let key in user4){
    // console.log(key);
    if(user1.hasOwnProperty(key)){
        console.log(key);
    }
}




let numbers = [1,2,3];
// console.log(Object.getPrototypeOf(numbers));
console.log(Array.prototype);
console.log(numbers);

function hello(){
    console.log("hello");
}





// class keyword 
class CreateUser{
    constructor(firstName, lastName, email, age, address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }
    about(){
        return `${this.firstName} is ${this.age} years old.`;
    }
    is18(){
        return this.age >= 18;
    }
    sing(){
        return "la la la la ";
    }

}
const user7 = CreateUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user8 = CreateUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user9 = CreateUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
console.log(Object.getPrototypeOf(user7));






//class
class Animal {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <= 1;
    }

    isCute(){
        return true;
    }
}

class Dog extends Animal{
    
} 

const tommy = new Dog("tommy", 3);
console.log(tommy);
console.log(tommy.isCute());





//super keyword

class Animal {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <= 1;
    }

    isCute(){
        return true;
    }
}

class Dog extends Animal{
    constructor(name, age, speed){
        super(name,age);
        this.speed = speed;
    }

    run(){
        return `${this.name} is running at ${this.speed}kmph`
    }
} 
// object / instance 
const snoopy = new Dog("snoopy", 3,45);
console.log(snoopy.run());







// same method in subclass
class Animal {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }

    eat(){
        return `${this.name} is eating`;
    }

    isSuperCute(){
        return this.age <= 1;
    }

    isCute(){
        return true;
    }
}

class Dog extends Animal{
    constructor(name, age, speed){
        super(name,age);
        this.speed = speed;
    }

    eat(){
        return `Modified Eat : ${this.name} is eating`
    }

    run(){
        return `${this.name} is running at ${this.speed}kmph`
    }
} 
const animal1 = new Animal('sheru', 2);
console.log(animal1.eat());






// getter and setters 
class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
const person1 = new Person("Sathelli", "DHileep", 22);
// console.log(person1.fullName());
// console.log(person1.fullName);
// person1.fullName = "Dhileep kumar";
// console.log(person1);






// static methods and properties
class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    static classInfo(){
        return 'this is person class';
    }
    static desc = "static property";
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName = lastName;
    }
    eat(){
        return `${this.firstName} is eating`;
    }

    isSuperCute(){
        return this.age <= 1;
    }

    isCute(){
        return true;
    }
}

const person2 = new Person("Dhileep", "kumar",22);
// // console.log(person1.eat());
// const info = Person.classInfo();
// console.log(person1.desc);
// console.log(info);















