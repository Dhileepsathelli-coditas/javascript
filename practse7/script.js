// iterables :string and array are iterable
const firstName = "Dhileep";
for(let char of firstName){
    console.log(char);
}
const items = ['item1', 'item2', 'item3'];
for(let item of items){
    console.log(item);
}
const first_Name = "Dhileep";
console.log(first_Name.length);
console.log(first_Name[2]);



// Sets:it is unordered collectioin of data.sets are iterable and stores data .it doesnot involve in indexing .
const item = ['item1', 'item2', 'item3'];
const numbers = new Set();
numbers.add(1);
numbers.add(2);
numbers.add(3);
numbers.add(4);
numbers.add(5);
numbers.add(6);
numbers.add(item);
if(numbers.has(1)){
    console.log("1 is present")
}else{
    console.log("1 is not present")
}
for(let number of numbers){
    console.log(number);
}
const myArray = [5,3,6,3,5,35,7,5,1,2,1];
const uniqueElements = new Set(myArray);
let length = 0;
for(let element of uniqueElements){
    length++;
}
console.log(length);




// Maps : it is iterable and stores data in ordered.
const Person = {
    firstName : "Dhileep",
    age: 22,
    1:"one"
}
console.log(Person.firstName);
console.log(Person["firstName"]);
console.log(Person[1]);
for(let key in Person){
    console.log(typeof key);
}
const person = new Map();
person.set('firstName', 'Dhileep');
person.set('age', 22);
person.set(1,'one');
person.set([1,2,3],'onetwothree');
person.set({1: 'one'},'onetwothree');
console.log(person);
console.log(person.get(1));
for(let key of person.keys()){
    console.log(key, typeof key);
}
for(let [key, value] of person){
    console.log(Array.isArray(key));
    console.log(key, value)
}

const person1 = {
    id: 1,
    firstName: "Dhileep"
}
const person2 = {
    id: 2,
    firstName: "Akhila"
}
const extraInfo = new Map();
extraInfo.set(person1, {age: 15, gender: "male"});
extraInfo.set(person2, {age: 12, gender: "female"});
console.log(person1.id);
console.log(extraInfo.get(person1).gender);
console.log(extraInfo.get(person2).gender);


// clone using Object.assign 
const obj = {
    key1: "value1",
    key2: "value2"
}
const obj3 = {'key69': "value69",...obj};
const obj4 = Object.assign({'key69': "value69"}, obj3);
obj.key3 = "value3";
console.log(obj3);
console.log(obj4);




// optional chaining 

const user  = {
    firstName: "Dileep",
    address: {houseNumber: '7-97/a'}
}
console.log(user?.firstName);
console.log(user?.address?.houseNumber);




// methods: function inside object

function personInfo(){
    console.log('person name is ${this.firstName} and age is ${this.age}');
}
const person3 = {
    firstName : "Dileep",
    age: 22,
    about: personInfo
}
const person4 = {
    firstName : "Akhila",
    age: 18,
    about: personInfo
}
const person5 = {
    firstName : "sanath",
    age: 19,
    about: personInfo
}
person3.about();
person4.about();
person5.about();
function about(hobby, favHero){
    console.log(this.firstName, this.age, hobby, favHero);
}
const user4 = {
    firstName : "Dileep",
    age: 22,   
}
const user6 = {
    firstName : "Akhila",
    age: 19,
    
}

console.log(window);
// "use strict";
function myFunc(){
    
     console.log(this);
 }
 myFunc();



 function about(hobby, Hero){
    console.log(this.firstName, this.age, hobby, Hero);
}
const user1 = {
    firstName : "Dhileep",
    age: 22,   
}
const user2 = {
    firstName : "kumar",
    age: 22,
    
}
// apply
about.apply(user1, ["photography", "Bunny"]);
const func = about.bind(user2, "Travelling", "NTR");
func();





const user3 = {
    firstName : "Dhileep",
    age: 22,
    about: function(){
        console.log(this.firstName, this.age);
    }   
}
const my_Func = user3.about.bind(user3);
my_Func();




// arrow functions :it is used to shorten the function code
const arrow = {
    firstName : "Dhileep",
    age: 22,
    about: () => {
        console.log(this.firstName, this.age);
    }   
}
arrow.about(arrow);


const user5 = {
    firstName : "kumar",
    age: 22,
    about(){
        console.log(this.firstName, this.age);
    }   
}
user5.about();



const userMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = {};
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    user.about = userMethods.about;
    user.is18 = userMethods.is18;
    return user;
}
const user8 = createUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user9 = createUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user10 = createUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
console.log(user8.about());
console.log(user10.about());



const user_Methods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'hai re hai re' ;
    }
}
function createUser(firstName, lastName, email, age, address){
    const user = Object.create(userMethods);// {}
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
const user11 = createUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user12 = createUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user13 = createUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
console.log(user11);
console.log(user11.about());




const obj1 = {
    key1: "value1",
    key2: "value2"
}
// prototype    
const obj2 = Object.create(obj1); // {}
// there is one more way to create empty object
obj2.key3 = "value3";
// obj2.key2 = "unique";
console.log(obj2);
console.log(obj2.__proto__);




function hello(){
    console.log("hello world");
}
console.log(hello.name);
hello.myOwnProperty = "very unique value";
console.log(hello.myOwnProperty);
console.log(hello.prototype); 
hello.prototype.abc = "abc";
hello.prototype.xyz = "xyz";
hello.prototype.sing = function(){
  return "hi re hi";
};
console.log(hello.prototype.sing());





const UserMethods = {
    about : function(){
        return `${this.firstName} is ${this.age} years old.`;
    },
    is18 : function(){
        return this.age >= 18;
    },
    sing: function(){
        return 'ola olala ';
    }
}
function createUsers(firstName, lastName, email, age, address){
    const user = Object.create(createUsers.prototype);
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.age = age;
    user.address = address;
    return user;
}
createUsers.prototype.about = function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUsers.prototype.is18 = function (){
    return this.age >= 18; 
}
createUsers.prototype.sing = function (){
    return "olala olala ";
}
const user14 = createUser('Dhileep', 'sathelli', 'dhileep@gmail.com', 22, "my address");
const user15 = createUser('sanath', 'gattu', 'sanath@gmail.com', 19, "my address");
const user16 = createUser('pavan', 'vasa', 'pavan@gmail.com', 17, "my address");
console.log(user14);
console.log(user16.is18());







